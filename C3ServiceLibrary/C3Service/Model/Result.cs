﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C3Service.Model
{
    public class Result
    {
        public Array Collections { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }

    }
}
